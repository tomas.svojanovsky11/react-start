import React, {useEffect, useState} from 'react';
import {Container, Row, Table} from "react-bootstrap";
import {getUsers} from "../api/users";

// Named export
// Default export

// export default function SlepiceLeti() {
export function SlepiceLeti() {
    const [users, setUsers] = useState([]);
    let [ahoj, setAhoj] = useState("Ahoj");

    // let ahoj = "Ahoj";

    useEffect(() => {
        (async () => {
            const result = await getUsers();
            setUsers(result);
        })();
    }, [])

    function changeAhojText(e) {
        setAhoj(e.target.value);
    }

    return (
        <Container>
            <Row>
                <h1>{ahoj}</h1>

                <input type="text" onKeyUp={changeAhojText}/>

                <Table striped bordered hover>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Age</th>
                    </tr>
                    </thead>
                    <tbody>
                    {users.map((user, index) => (
                        <tr key={user.id}>
                            <td>{index + 1}</td>
                            <td>{user.firstName}</td>
                            <td>{user.lastName}</td>
                            <td>{user.age}</td>
                        </tr>
                    ))}
                    </tbody>
                </Table>
            </Row>
        </Container>
    );
}
