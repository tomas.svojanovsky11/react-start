import axios from "axios";

export function getUsers() {
    return axios("http://localhost:3001/users")
        .then(res => res.data);
}
