// const user = {
//     name: "Tomas",
//     lastName: "Svojanovsky",
//     getName() {
//         // nejaka dodatecna logika
//         //
//         // return () => {
//         //     return `${this.name} ${this.lastName}`;
//         // }
//
//         return `${this.name} ${this.lastName}`;
//     },
// };
//
// user.getName();

// class User {
//     constructor(name, surname) {
//         this.name = name;
//         this.surname = surname;
//     }
//
//     getName() {
//         return `${this.name} ${this.surname}`;
//     }
// }

function User(name, surname) {
    this.name = name;
    this.surname = surname;
}

User.prototype.getName = function () {
    return `${this.name} ${this.surname}`
}

const user = new User("pepa", "zdepa");

console.log(user.getName());
// let user = new User("pepa", "zdepa");

// function trick(callback) {
//     console.log(callback());
// }
//
// trick(user.getName);


// let getter = user.getName();
// console.log(getter());

// class Man {
//
// }
//
// class User extends Man {
//     constructor(name, surname) {
//         super();
//     }
//
//     getName() {
//
//     }
// }
//
// let user = new User("pepa", "zdepa");
//
// console.log("Prvni");
//
// const user = {
//     name: "Tomas",
//     surname: "Svojanovsky",
// };

// fetch("http://localhost:3001/users", {
//     method: "get",
// }).then(response => {
//     return response.json();
// }).then(result => {
//         console.log(result);
// }).catch(error => {
//  console.log(error);
// });

// async function main() {
//     console.log("prvni");
//
//     try {
//         let response = await fetch("http://localhost:3001/users", {
//             method: "get",
//         });
//
//         console.log("Druhy");
//
//         let users = await response.json();
//         console.log(users);
//     } catch(e) {
//         console.log("Something went wrong");
//     } finally {
//         console.log("Uplne na konec bez ohledu jestli je error nebo ne");
//     }
//
//     console.log("treti");
// }
//
// main();

// setTimeout(() => { // Callback queue
//     console.log("set timeout");
// }, 0);

// new Promise((resolve, reject) => { // Microtask queue
//     reject("ahoj");
// }).then((value) => {
//    console.log(value);
// }).catch(value => {
//     console.log(`Error: ${value}`);
// });

// console.log("Posledni");

// resolve -> then
// reject -> catch

// console.log(value);
